.. dlmontepython documentation master file, created by
   sphinx-quickstart on Thu Apr  9 16:47:40 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to dlmontepython's documentation!
=========================================

.. toctree::
   :maxdepth: 2

   dlmontepython

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
